""" Tools for importing known issues from SQUAD """

from squad_client.core.models import Squad


KNOWNISSUES_KEYS = (
    'id', 'title', 'test_name', 'url', 'notes', 'active', 'intermittent'
)


def get_obj_list(obj_type):
    """ Get object items from SQUAD """
    return list(getattr(Squad(), obj_type)(count=-1).items())


def obj_from_items(url, obj_list):
    """ Get object from SQUAD using items """
    obj_id = url.split('/')[-2]
    try:
        return next( value for key, value in obj_list if key == int(obj_id))
    except StopIteration as exc:
        raise KeyError(f'ID {obj_id} not found in ojbect list!') from exc


def env_project_name(urls):
    """ project and environment slug from environment id """
    environments = get_obj_list('environments')
    projects = get_obj_list('projects')
    groups = get_obj_list('groups')
    for url in urls:
        environment = obj_from_items(url, environments)
        project = obj_from_items(environment.project, projects)
        group = obj_from_items(project.group, groups)
        yield group.slug, project.slug, environment.slug


def get_known_issues():
    """ Get known issues from SQUAD in human reable format """

    for ki in Squad().knownissues().values():
        groups = {}
        for group_slug, project_slug, env_slug in env_project_name(ki.environments):
            group = groups.setdefault(group_slug, {'projects': {}})
            project = group['projects'].setdefault(project_slug, {'environments': []})
            if env_slug not in project['environments']:
                project['environments'].append(env_slug)
            group['projects'][project_slug] = project
            groups[group_slug] = group
        test = {key: getattr(ki, key) for key in KNOWNISSUES_KEYS}
        yield dict(test, groups=groups)
