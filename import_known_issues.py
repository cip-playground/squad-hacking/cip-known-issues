""" Script for importing known issues from SQUAD to a yaml file """

import yaml
import known_issue_importer as kii


def main():
    """ Main function """
    known_issues = sorted(list(kii.get_known_issues()), key=lambda x: x['id'])
    with open('known_issues.yaml', 'w', encoding='utf-8') as file:
        yaml.dump(known_issues, file, sort_keys=False, default_flow_style=False)


if __name__ == '__main__':
    main()
