# CIP Known Issues

This repository automates the creation of known issues for CIP SQUAD.

Known issues are documented in a central `YAML` file. If this file is changed, known issues in SQUAD are modified accordingly, using a GitLab Pipeline.

## YAML structure

What follows is an example of how the `YAML` with known issues is structured

```yaml
- id: 1
  title: Spectre Meltdown Test CVE-2018-3640
  test_name: spectre-meltdown-checker-test/CVE-2018-3640
  url: null
  notes: ''
  active: true
  intermittent: false
  groups:
    cip-kernel:
      projects:
        linux-6.1.y-cip:
          environments:
          - arm_renesas_shmobile_defconfig_r8a7743-iwg20d-q7
          - arm64_renesas_defconfig_r8a774a1-hihope-rzg2m-ex
        linux-5.10.y-cip:
          environments:
          - x86_siemens_ipc227e_defconfig_x86-simatic-ipc227e
          - arm64_renesas_defconfig_r8a774a1-hihope-rzg2m-ex
          - arm_renesas_shmobile_defconfig_r8a7743-iwg20d-q7
    linux-cip:
      projects:
        linux-6.1.y-cip:
          environments:
          - arm_renesas_shmobile_defconfig_r8a7743-iwg20d-q7
          - arm64_renesas_defconfig_r8a774a1-hihope-rzg2m-ex
        linux-6.1.y-cip-rt:
          environments:
          - arm_renesas_shmobile_defconfig_r8a7743-iwg20d-q7
          - arm64_renesas_defconfig_r8a774a1-hihope-rzg2m-ex
          - arm64_renesas-rt_defconfig_r8a774a1-hihope-rzg2m-ex
          - arm_renesas_shmobile-rt_defconfig_r8a7743-iwg20d-q7
- id: 2
  title: Spectre Meltdown Test CVE-2018-12126
  test_name: spectre-meltdown-checker-test/CVE-2018-12126
  url: null
  notes: ''
  active: true
  intermittent: false
  groups:
    cip-kernel:
      projects:
        linux-6.1.y-cip:
          environments:
          - x86_siemens_ipc227e_defconfig_x86-simatic-ipc227e
        linux-5.10.y-cip:
          environments:
          - x86_siemens_ipc227e_defconfig_x86-simatic-ipc227e
    linux-cip:
      projects:
        linux-6.1.y-cip:
          environments:
          - x86_siemens_ipc227e_defconfig_x86-simatic-ipc227e
        linux-4.19.y-cip:
          environments:
          - x86_siemens_ipc227e_defconfig_x86-simatic-ipc227e
```

## Known Issues in SQUAD

The following describes how known issues can be created, where the are listed and what effect they have on test results.

### Properties

- Required fields:
  - test suite and test name: `<suite-name>/<test-name>`, e.g.
    `spectre-meltdown-checker-test/CVE-2018-3640`
  - One or more group/project/environment combinations:
    `<group-name>/<project-name> - <environment-name>`, e.g.
    `linux-cip/linux-6.1.y-cip - arm_renesas_shmobile_defconfig_r8a7743-iwg20d-q7`
- Optional fields:
  - URL
  - Notes

### Manage known issues using the Admin UI

Known issues can also be created using SQUAD's administration panel. From there,
navigate to **Known Issues**. Here, at the top right, choose **Add known issue**
to add a known issue. A formula will appear, in which all the (required) known
issues properties can be entered.

### Manage known issues using the REST API

The SQUAD REST API also offer the possibility to [create Known
Issues](https://squad.readthedocs.io/en/latest/api.html#knownissues-api-knownissues).

This repository provides the module [known_issues_tools](known_issues_tools.py), which can be used to create known issues from a human readable `YAML` file. To replace the existing known issues in SQUAD, use the module [rest_api_tools](rest_api_tools.py). Please refer to the main script [export_known_issues](export_known_issues.py) on how to use the modules.

### Known issue overview

After a known issues has been created, it is visible under known issues tab.
This give an overview of tests with known issues (see e.g.
[here](https://squad.ciplatform.org/cip-kernel/linux-6.1.y-cip/knownissues/))

### Test results with known issues

Test result reporting, after a known issue has been created, are marked as
`xfail`. See for example [this
result](https://squad.ciplatform.org/cip-kernel/linux-6.1.y-cip/build/6.1.62-cip9_4441e8879/testrun/11508/suite/spectre-meltdown-checker-test/test/CVE-2018-3640/details/)

Known issues are not registered at previously reported tests. E.g. [this
test](https://squad.ciplatform.org/api/tests/169542/) has known issues, but is
not registered.
