""" Compare SQUAD output with local YAML file """

import os
import json
import yaml
import jsondiff
from squad_client.core.api import SquadApi
import known_issues_tools as kit
import rest_api_tools as rat


def get_env_variable(var_name, default_value=None):
    """ Get environment variable """
    try:
        return os.environ[var_name]
    except KeyError as exc:
        if default_value is not None:
            return default_value
        raise KeyError(f'{var_name} not set in environment!') from exc


def sort_json(json_data):
    """ Sort JSON data """
    json_data['results'] = sorted(json_data['results'], key=lambda x: x['id'])
    for result in json_data['results']:
        result['environments'] = sorted(result['environments'], key=lambda x: int(x.split('/')[-2]))
    return json_data


def compare_jsons(json1, json2):
    """ Compare two JSON objects """
    baseline = sort_json(json1)
    imported = sort_json(json2)
    baseline_str = json.dumps(baseline, sort_keys=True, indent=2)
    import_str = json.dumps(imported, sort_keys=True, indent=2)
    return jsondiff.diff(baseline_str, import_str, syntax='symmetric')


def main():
    """ Main function """
    squad_url = get_env_variable('SQUAD_URL', 'https://squad.ciplatform.org')
    squad_token = get_env_variable('SQUAD_TOKEN')
    SquadApi.configure(url=squad_url)

    with open('known_issues.yaml', 'r', encoding='utf-8') as file:
        known_issues = yaml.safe_load(file)
    known_issues_rest = kit.create_rest_known_issues(known_issues)

    print('Comparison check:')
    api = rat.SQUADRestApi(squad_url=squad_url, token=squad_token)
    print('Differences between known issues in SQUAD and local file:')
    diff = compare_jsons(api.get(urlend='knownissues'), known_issues_rest)

    if not diff:
        print('No differences found!')
    else:
        print('There are difference!!\n')
        print('YAML file')
        print(json.dumps(sort_json(known_issues_rest),
                         sort_keys=True, indent=2))
        print('SQUAD')
        print(json.dumps(sort_json(api.get(urlend='knownissues')),
                         sort_keys=True, indent=2))

if __name__ == '__main__':
    main()
