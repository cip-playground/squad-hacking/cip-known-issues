""" REST API Tools for SQUAD """

import requests


class AuthenticationError(Exception):
    """ Authentication Error """


class SQUADRestApi:
    """ REST API Tools for SQUAD """

    def __init__(self, squad_url, token):
        self.squad_url = squad_url
        self.token = token
        self.session = requests.Session()
        self.session.headers.update(
            {'Authorization': f'Token {self.token}'}
        )

    def _call(self, method, **kwargs):
        """ General shorthand or API Calls """
        kwargs.setdefault('timeout', 10)
        url = self.squad_url
        if urlend := kwargs.pop('urlend'):
            url = f'{self.squad_url}/api/{urlend}'
        response = self.session.request(method, url, **kwargs)
        if response.status_code != 200:
            raise AuthenticationError(response.text)
        return response

    def get(self, **kwargs):
        """ Shorthand for API get calls """
        return self._call('get', **kwargs).json()

    def post(self, **kwargs):
        """ Shorthand for API post calls """
        return self._call('post', **kwargs)

    def put(self, **kwargs):
        """ Shorthand for API put calls """
        return self._call('put', **kwargs)
