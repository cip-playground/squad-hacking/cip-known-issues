"""
Export known issues from SQUAD to a yaml file
"""

import os
import json
import yaml
from squad_client.core.api import SquadApi
import known_issues_tools as kit
import rest_api_tools as rat


def get_env_variable(var_name, default_value=None):
    """ Get environment variable """
    try:
        return os.environ[var_name]
    except KeyError as exc:
        if default_value is not None:
            return default_value
        raise KeyError(f'{var_name} not set in environment!') from exc


def main():
    """ Main function """
    squad_url = get_env_variable('SQUAD_URL', 'https://squad.ciplatform.org')
    squad_token = get_env_variable('SQUAD_TOKEN')
    SquadApi.configure(url=squad_url)

    with open('known_issues.yaml', 'r', encoding='utf-8') as file:
        known_issues = yaml.safe_load(file)
    known_issues_rest = kit.create_rest_known_issues(known_issues)

    api = rat.SQUADRestApi(squad_url=squad_url, token=squad_token)
    response = api.put(
        urlend='knownissues',
        data=json.dumps(known_issues_rest),
        headers={'Content-Type':'application/json; charset=UTF-8'},
    )
    print(response.status_code)

if __name__ == '__main__':
    main()
