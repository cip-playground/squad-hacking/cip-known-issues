""" Known Issues Tools for SQUAD-client """

from squad_client.core.models import Squad


KNOWNISSUES_BASE = {
    "count": 0,
    "next": None,
    "previous": None,
    "results": [],
}

KNOWNISSUES_KEYS_SQUAD = (
    'url', 'title', 'test_name', 'notes', 'active', 'intermittent'
)


def get_obj_list(obj_type):
    """ Get object items from SQUAD """
    return list(getattr(Squad(), obj_type)(count=-1).items())


def gather_environments(ki_entry):
    """ Gather environments from known issue entry """
    for group_slug, group in ki_entry['groups'].items():
        for project_slug, project in group['projects'].items():
            for env in project['environments']:
                yield group_slug, project_slug, env


def obj_attr_checker(obj, attr_values):
    """ Check if all object attributes match the given values """
    for attr, value in attr_values.items():
        if getattr(obj, attr) != value:
            return False
    return True


def get_obj_from_list(obj_list, **attr_values):
    """ Get object from list using attribute and value """
    try:
        return next(obj for _, obj in obj_list if obj_attr_checker(obj, attr_values))
    except StopIteration as exc:
        raise KeyError(f'Object with {attr_values} not found!') from exc


def get_env_urls(groups, projects, environments, ki_entry):
    """ Get environment urls from list of environment slugs """
    for group_slug, project_slug, env_slug in gather_environments(ki_entry):
        group = get_obj_from_list(groups, slug=group_slug)
        project = get_obj_from_list(projects, slug=project_slug, group=group.url)
        env = get_obj_from_list(environments, slug=env_slug, project=project.url)
        yield env.url


def create_rest_known_issues(known_issues):
    """ From human readable known issues in dict format """
    ki_dict = dict(KNOWNISSUES_BASE)
    groups = get_obj_list('groups')
    projects = get_obj_list('projects')
    environments = get_obj_list('environments')

    for index, ki in enumerate(known_issues):
        new_entry = {'id': index + 1} \
                  | {key: ki[key] for key in KNOWNISSUES_KEYS_SQUAD}
        urls = list(set(get_env_urls(groups, projects, environments, ki)))
        new_entry['environments'] = sorted(urls, key=lambda x: int(x.split('/')[-2]))
        ki_dict['results'].append(new_entry)
        ki_dict['count'] += 1
    return ki_dict
